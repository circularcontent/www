<div class='octoolbox-project-client'> 
<?php  
        global $post;     
        $client_id = octoolbox_get_project_metadata_value( $post->ID, 'client_id' );
        if( ! is_wp_error( $client_id ) ) {
            $client_name        = get_the_title( $client_id );  
            $client_image       = get_the_post_thumbnail( $client_id ); 
            $client_description = get_the_excerpt( $client_id ); 
        }     

    ?>
    <h2>Client: <?php echo $client_name; ?></h2>
    <p><?php echo $client_image; ?></p>
    <p><?php echo $client_description; ?></p>
</div>
