<div class='octoolbox-project-products'> 
    <?php  
  
    function get_project_term_id( $post_id ) {
        $terms = get_the_terms( $post_id, 'project'); 
        if( is_array( $terms ) && sizeof( $terms ) > 0 ) {
            return $terms[0]->term_id; 
        }
    } 

    // retrieve the products belonging to this project
    // TODO: methods like this should be made available in an easy way for themers
    // Copied from class-project :( and slightly changed. Need to refactor
    function get_products( $post ) {
        $term_id = get_project_term_id( $post->ID );   
        $defaults = array( 
            'posts_per_page' => '-1',
            'post_status'    => 'publish',  
            'post_type'      => 'oct-product',
            'tax_query'      => array(
                array( 
                    'taxonomy' => 'project',
                    'field'    => 'term_id',
                    'terms'    => $term_id
                )
            )
        );
        $products = get_posts( $defaults ); 
        return $products; 
    } 

    function render_product_people( $product_post, $seperator = ', ' ) {
        $term_id   = Util::get_selected_term_id( $product_post->ID, 'product' );
        $people = Util::get_posts_by_term( $term_id, 'product', 'oct-person' );
        $output = '';
        if( is_array( $people ) && sizeof( $people ) > 0 ) {
            foreach( $people as $person ) {
                $output .= $person->post_title . $seperator;       
            }
            $output = rtrim( $output, $seperator ); 
        }
        return $output;
    }

    
    function render_products() {
        global $post;
        $products = get_products( $post );
        if( is_array($products) && sizeof( $products ) > 0 ) { 
            render_products_table( $products );
            render_products_paragraphes( $products );
        } else {
            echo 'No products found';    
        }
    }


    function render_products_table( $products) {
        $html =''; 
        $html .= "<p>";
        $html .= "<table class='octoolbox-project-products' id='octoolbox_project_products'>";
        $html .= '<thead>';
        $html .= "<tr>";
        $html .="<th><strong>Title</strong></th>";
        $html .="<th><strong>License</strong></th>";
        $html .="<th><strong>Download</strong></th>";
        $html .="<th><strong>More</strong></th>";
        $html .="</tr>";
        $html .= '</thead>';
        $html .= '<tbody>';
        foreach( $products as $p ) {
            $download_url = Util::get_post_metadata_value( '_octoolbox_product_metadata', $p->ID, 'download_url' );
            $download_url = empty( $download_url) || is_wp_error( $download_url)  ?  'Currently not available' : "<a href='$download_url'>$download_url</a>";  
            $html .= "<tr>";
            $html .="<td>" . get_the_title( $p ) . "</td>";
            $html .="<td>" . Util::get_license_url( $p ) . "</td>";
            $html .="<td>$download_url</td>";
            $html .="<td><a href='#product-$p->ID'>Details</a></td>";
            $html .="</tr>";
        }    
        $html .= '</tbody>';
        $html .= '</table>';
        echo $html;    
    } 
    

    function render_products_paragraphes( $products ) {
        $html ='';    
        foreach( $products as $p ) {
            $download_url = Util::get_post_metadata_value( '_octoolbox_product_metadata', $p->ID, 'download_url' );
            $download_url = empty( $download_url) || is_wp_error( $download_url)  ?  '' : "Download URL: <a href='$download_url'>$download_url</a>";  
            $html .="<h3 id='product-$p->ID'>" . get_the_title( $p ) . "</h3>";
            $html .= get_the_post_thumbnail( $p->ID ); 
            $html .= "<ul style='list-style: none;'>"; 
            $html .="<li>Licensed: " . Util::get_license_url( $p ); 
            $html .=" by " . render_product_people( $p ); 
            $html .="</li>";
            $html .="<li>$download_url</li>";
            $html .="</ul>";
            $html .="<p>" . get_the_content( $p ) . "</p>";
        }    
        echo $html;    
    }  

    ?>
    <h2>Products</h2>
    <?php render_products(); ?>

</div>
