<?php

  // # TODO refactor 
  function octoolbox_get_project_metadata_value( $post_id, $key ) {
    $metadata = get_post_meta( $post_id, '_octoolbox_project_metadata', false);   
    if( is_array( $metadata ) && sizeof( $metadata ) > 0 ) {
      foreach( $metadata as $data_array ) {
        if( array_key_exists( $key, $data_array ) ) {
          return $data_array[ $key ]; 
        } else {
          return null;
        }
      }
    } else {
      return new WP_Error( 'octoolbox', __( 'No metadata found', 'YOUR-TEXTDOMAIN') );
    }
  }
 
  function octoolbx_enqueue_styles() { 

    $parent_style = 'twentyseventeen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'octoolbox-style',
      get_stylesheet_directory_uri() . '/style.css',
      array( $parent_style ),
      wp_get_theme()->get('Version')
    );
  }
  add_action( 'wp_enqueue_scripts', 'octoolbx_enqueue_styles' );
?>
