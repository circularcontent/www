<?php 
/** 
 * A CPT Project will automagically create a CT Project and/or register itself to this taxonomy
 * Products will register themselves to the same taxonomy, this way will relate a project with one or more products. 
 * The CPT Project is only used for the Project metadata.
**/    



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OCProject {

  function __construct() {
    
    add_action( 'save_post_oct-project', array( $this, 'link_with_taxonomy' ), 10, 3);
    
    add_action( 'save_post_oct-project', array( $this, 'save_metaboxes' ), 11 );
    add_action( 'edit_post_oct-project', array( $this, 'save_metaboxes' ), 11 );
    
    add_action( 'save_post_oct-project', array( $this, 'maybe_remove_products' ), 11, 1 );
    add_action( 'save_post_oct-project', array( $this, 'maybe_update_products' ), 11, 1 );
  }



  // custom taxonomy to combine projects products
  function ct_oct_project_init() {
    register_taxonomy( 'project', array( 'oct-project', 'oct-product' ), array(
      'hierarchical'      => true,
      'public'            => true,
      'show_in_nav_menus' => false,
      'show_ui'           => false,
      'show_admin_column' => true,
      'query_var'         => true,
      'rewrite'           => true,
      'capabilities'      => array(
        'manage_terms'  => 'edit_posts',
        'edit_terms'    => 'edit_posts',
        'delete_terms'  => 'edit_posts',
        'assign_terms'  => 'edit_posts'
      ),
      'labels'            => array(
        'name'                       => __( 'Projects', 'YOUR-TEXTDOMAIN' ),
        'singular_name'              => _x( 'Project', 'taxonomy general name', 'YOUR-TEXTDOMAIN' ),
        'search_items'               => __( 'Search projects', 'YOUR-TEXTDOMAIN' ),
        'popular_items'              => __( 'Popular projects', 'YOUR-TEXTDOMAIN' ),
        'all_items'                  => __( 'All projects', 'YOUR-TEXTDOMAIN' ),
        'parent_item'                => __( 'Parent project', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'          => __( 'Parent project:', 'YOUR-TEXTDOMAIN' ),
        'edit_item'                  => __( 'Edit project', 'YOUR-TEXTDOMAIN' ),
        'update_item'                => __( 'Update project', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'               => __( 'New project', 'YOUR-TEXTDOMAIN' ),
        'new_item_name'              => __( 'New project', 'YOUR-TEXTDOMAIN' ),
        'separate_items_with_commas' => __( 'Projects separated by comma', 'YOUR-TEXTDOMAIN' ),
        'add_or_remove_items'        => __( 'Add or remove projects', 'YOUR-TEXTDOMAIN' ),
        'choose_from_most_used'      => __( 'Choose from the most used projects', 'YOUR-TEXTDOMAIN' ),
        'not_found'                  => __( 'No projects found.', 'YOUR-TEXTDOMAIN' ),
        'menu_name'                  => __( 'Projects', 'YOUR-TEXTDOMAIN' ),
      ),
      'show_in_rest'      => true,
      'rest_base'         => 'project',
      'rest_controller_class' => 'WP_REST_Terms_Controller',
    ) );

  }

  // Create oct cpt project
  function cpt_oct_project_init() {
    register_post_type( 'oct-project', array(
      'labels'            => array(
        'name'                => __( 'Projects', 'YOUR-TEXTDOMAIN' ),
        'singular_name'       => __( 'Project', 'YOUR-TEXTDOMAIN' ),
        'all_items'           => __( 'All Projects', 'YOUR-TEXTDOMAIN' ),
        'new_item'            => __( 'New project', 'YOUR-TEXTDOMAIN' ),
        'add_new'             => __( 'Add New', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'        => __( 'Add New project', 'YOUR-TEXTDOMAIN' ),
        'edit_item'           => __( 'Edit project', 'YOUR-TEXTDOMAIN' ),
        'view_item'           => __( 'View project', 'YOUR-TEXTDOMAIN' ),
        'search_items'        => __( 'Search projects', 'YOUR-TEXTDOMAIN' ),
        'not_found'           => __( 'No projects found', 'YOUR-TEXTDOMAIN' ),
        'not_found_in_trash'  => __( 'No projects found in trash', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'   => __( 'Parent project', 'YOUR-TEXTDOMAIN' ),
        'menu_name'           => __( 'OC Toolbox', 'YOUR-TEXTDOMAIN' ),
      ),
      'public'            => true,
      'hierarchical'      => false,
      'show_ui'           => true,
      'show_in_nav_menus' => true,
      'supports'          => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions' ),
      'register_meta_box_cb' => array( $this, 'cpt_oct_project_metaboxes'), 
      'has_archive'       => true,
      'rewrite'           => true,
      'query_var'         => true,
      'menu_icon'         => 'dashicons-chart-pie',
      'show_in_rest'      => true,
      'rest_base'         => 'oct-project',
      'rest_controller_class' => 'WP_REST_Posts_Controller',
    ) );

  }


  
  function cpt_oct_project_updated_messages( $messages ) {
    global $post;

    $permalink = get_permalink( $post );

    $messages['oct-project'] = array(
      0 => '', // Unused. Messages start at index 1.
      1 => sprintf( __('Project updated. <a target="_blank" href="%s">View project</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      2 => __('Custom field updated.', 'YOUR-TEXTDOMAIN'),
      3 => __('Custom field deleted.', 'YOUR-TEXTDOMAIN'),
      4 => __('Project updated.', 'YOUR-TEXTDOMAIN'),
      /* translators: %s: date and time of the revision */
      5 => isset($_GET['revision']) ? sprintf( __('Project restored to revision from %s', 'YOUR-TEXTDOMAIN'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
      6 => sprintf( __('Project published. <a href="%s">View project</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      7 => __('Project saved.', 'YOUR-TEXTDOMAIN'),
      8 => sprintf( __('Project submitted. <a target="_blank" href="%s">Preview project</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
      9 => sprintf( __('Project scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview project</a>', 'YOUR-TEXTDOMAIN'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
      10 => sprintf( __('Project draft updated. <a target="_blank" href="%s">Preview project</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true'
      , $permalink ) ) ),
    );

    return $messages;

  }
  
  // cpt oct project metaboxes 
  function cpt_oct_project_metaboxes( $post ) {
    add_meta_box( 'oct-client', 'Client', array( $this, 'mbx_render_client' ), null, 'side', 'high', null );  
    add_meta_box( 'oct-product', 'Products', array( $this, 'mbx_render_product' ), null, 'normal', 'high', null );  
  }


  function mbx_render_product( $post ) {
    $products   = $this->get_products( $post );    
    
    $html = wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce', true, false );

    $html .= "<p>";
    $html .= "<table class='widefat wp-list-table striped' id='octoolbox_project_products'>";
    $html .= '<thead>';
    $html .= "<tr>";
    $html .="<th><strong>Project phase</strong></th>";
    $html .="<th><strong>Title</strong></th>";
    $html .="<th><strong>License</strong></th>";
    $html .="<th><strong>Status</strong></th>";
    $html .="<th colspan='2'><strong>Actions</strong></strong></th>";
    $html .="</tr>";
    $html .= '</thead>';
    
    if( (is_array( $products) && sizeof( $products ) > 0 ) ) { 
      $html .= '<tbody>';
      foreach( $products as $key =>  $p ) {
        $edit_url = admin_url("/post.php?post=$p->ID&action=edit"); 
        
        $html .= "<tr>";
        $html .="<td>" . Util::get_terms_string( $p, 'phase' )  . "</td>";
        $html .="<td>" . get_the_title( $p ) . "</td>";
        $html .="<td>" . Util::get_terms_string( $p, 'license') . "</td>";
        $html .="<td>" . get_post_status( $p->ID ). "</td>";
        $html .="<td><a href='$edit_url' title='Edit product'>Edit</a>";
        $html .= Util::add_hidden_field('_octoolbox_project_existing_products[]', $p->ID ); 
        $html .= "</td>";
        $html .="<td><input type='checkbox' value='$p->ID' name='_octoolbox_project_remove_products[]'>Remove</td>";
        $html .="</tr>";
      }   
      $html .= '</tbody>';
    } 

    $html .= '<tfoot>';
    $html .= "<tr>";
    $html .= "<td>" . $this->render_product_phases() . "</td>";
    $html .= "<td><input type='text' data-required='required'  name='_octoolbox_project_metadata[products][product_name][]' placeholder='Product Name'></input></td>";
    $html .= "<td>" . $this->render_product_licenses() . "</td>";
    $html .= "<td></td>";
    $html .= "<td colspan='2'><input type='button' name='_octoolbox_project_add_product' id='octoolbox_project_add_nw_product' value='Add' class='button'></input></td>";
    $html .= "<tr>";
    $html .= '</tfoot>';
    $html .= "</table>"; 
    $html .="</p>";
    
    $html .="<p>";
    $html .= __("Easily add new products to your project. Do not forget to save your project to save your products. Use the project's edit button to add the rest of the necessary details");   
    $html .="</p>";
    echo $html;
  } 


  function mbx_render_client( $post ) {
    $client_url = admin_url('/post-new.php?post_type=oct-client'); // TODO add current project so I can add the new client to this project in one go

    wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce' );

    $client_id = Util::get_post_metadata_value( '_octoolbox_project_metadata', $post->ID, 'client_id' );
    $client_id = is_wp_error( $client_id ) ? null : $client_id; 

    echo "<label for='oct-project-client'>Select client: </label>"; 
    echo "<select name='_octoolbox_project_metadata[client_id]' id='oct-project-client'>";
    echo Util::retrieve_posts_as_options( 'oct-client', $client_id ); 
    echo "</select><br>"; 
    echo "Or <a href='$client_url'>add a new client</a>";
  }


  # TODO refactor: is part of product and project
  function render_product_phases() {
    $html = '';
    $html .= "<select name='_octoolbox_project_metadata[products][product_phase][]' id='oct-project-product-phase'>";
    $html .= Util::retrieve_terms_as_options( 'phase', 0 ); 
    $html .= "</select>";
    return $html;  
  }


  # TODO refactor: is part of product and project
  function render_product_licenses() {
    $html = '';
    $html .= "<select name='_octoolbox_project_metadata[products][product_license][]' id='oct-project-product-license'>";
    $html .= Util::retrieve_terms_as_options( 'license', 0 ); 
    $html .= "</select>";
    return $html;  
  }




  // link current CPT to taxonomy
  function link_with_taxonomy( $id, $post, $is_update ) {
    Util::add_cpt_to_taxonomy( $id, $post, 'project', false); 
  }


  // @todo add proper validation & sanitation!
  //
  function save_metaboxes( $post_id ) {
    
    if ( ! isset( $_POST['octoolbox_nonce'] ) ) {
      return $post_id;
    } 

    if( ! check_admin_referer( 'octoolbox_nonce', 'octoolbox_nonce' ) ) {
      return $post_id;
    }

    if( isset( $_POST[ '_octoolbox_project_metadata' ] ) ) {
      $data = $_POST[ '_octoolbox_project_metadata' ];
      $products = $this->cleanup_products( $data['products'] );
      if( ! is_wp_error( $products ) ) {
        $data['product_ids'] = $this->add_products( $products, $post_id ); 
        update_post_meta( $post_id, '_octoolbox_project_metadata', $data );
      }
    }
  }

  // remove products if
  // When the post or page is permanently deleted, everything that is tied to it is deleted also. This includes comments, post meta fields, and relationships between the post and taxonomy terms. @see https://codex.wordpress.org/Function_Reference/wp_delete_post
  function maybe_remove_products( $post_id ) {
    if( isset( $_POST['_octoolbox_project_remove_products'] ) ) {
      $remove_products_ids = $_POST['_octoolbox_project_remove_products'];
      Util::remove_multiple_posts( $remove_product_ids ); 
      return;
    }
  } 

  // updates existing products to use the most recent term_id
  function maybe_update_products( $post_id ) {
    if( isset( $_POST['_octoolbox_project_existing_products'] ) ) {
      $existing_product_ids = $_POST['_octoolbox_project_existing_products'];
      $remove_product_ids   = isset( $_POST['_octoolbox_project_remove_products'] )  ? $_POST['_octoolbox_project_remove_products'] : array(); 
      $products_ids = array_diff( $existing_product_ids, $remove_product_ids ); // only get the ids which are not supposed to be removed  
      if( is_array( $products_ids ) && sizeof( $products_ids ) > 0 ) {
        $project_term_id = $this->get_project_term_id( $post_id ); 
        foreach( $products_ids as $k => $product_id) {
          $result = wp_set_object_terms( $product_id, $project_term_id, 'project', false ); 
        } 
      }
      return;
    }
  }



  // retrieve the products belonging to this project
  // TODO: methods like this should be made available in an easy way for themers
  function get_products( $post ) {
    $term_id = $this->get_project_term_id( $post->ID );   
    $defaults = array( 
      'posts_per_page' => '-1',
      'post_status'    => 'any',  
      'post_type'      => 'oct-product',
      'tax_query'      => array(
        array( 
          'taxonomy' => 'project',
          'field'    => 'term_id',
          'terms'    => $term_id
        )
      )
    );
    $products = get_posts( $defaults ); 
    return $products; 
  } 



/*
  Als de data binnenkomt doe de volgende acties: 
    
    - Maak een product aan en koppel het product met het project via de oc-project taxonomy.
    - Koppel het product aan de license taxonomy, de term id krijg je al binnen
    - Koppel het product aan de phase taxonomy, de term id krijg je al binnen 
    - TODO nadenken over hoe je een persoon aan een product koppelt, want je moet de role ook meenemen??    
 */

  function add_products( $products, $post_id ) {
    if( is_array ( $products ) && sizeof( $products ) > 0 ) {
      $prod_ids = array(); 
      $project_term_id = $this->get_project_term_id( $post_id ); 
      foreach( $products as $product ) {
        if( is_array( $product ) && sizeof( $product) >0 ) {
          $product_post_data = array( 
            'post_type'   => 'oct-product',
            'post_status' => 'draft',
            'tax_input'   => array( 'project' => $project_term_id)
          ); 
          foreach( $product as $key => $val ) {

            switch( $key ) {
              case 'product_name': 
                $product_post_data[ 'post_title' ] = wp_strip_all_tags( $val ); 
              break;
                
              case 'product_phase': 
                $product_post_data[ 'tax_input' ] ['phase'] = $val; 
              break;

              case 'product_license': 
                $product_post_data[ 'tax_input' ] ['license'] = $val; 
              break;
            }
          } 
          //error_log( print_r( $product_post_data, true ) ) ;
          $id = wp_insert_post ( $product_post_data, true );
          //error_log( 'product_insert_id: ' . $id ); 
          if( ! is_wp_error( $id ) ) {
            $prod_ids[] = $id; 
          }  
        }      
      }
      return $prod_ids;  
    
    }
  } 


  function get_project_term_id( $post_id ) {
    return Util::get_selected_term_id( $post_id, 'project' );   
  } 


  /**
   * Clean up the products data before furter processing 
   *
   * @param array products data 
   * @return array|WP_Error cleaned up data or WP_Error  
   *
   * For future reference this is what the incoming data looks like: 
   *     
   *   PHP message: Array
   *   (
   *       [product_phase] => Array
   *           (
   *               [0] => 14
   *               [1] => 14
   *               [2] => 14
   *               [3] => 14
   *           )
   *
   *       [product_name] => Array
   *           (
   *               [0] => Needs to be removed
   *               [1] => Test 1
   *               [2] => Test 2
   *               [3] => Test 3
   *           )
   *
   *       [product_people] => Array
   *           (
   *               [0] => 236
   *               [1] => 236
   *               [2] => 236
   *               [3] => 236
   *           )
   *
   *       [product_license] => Array
   *           (
   *               [0] => 15
   *               [1] => 15
   *               [2] => 15
   *               [3] => 15
   *           )
   *
   *   )
   *
   * First we need to combine the product_* metadata per product 
   * instead of the other way around. After this it looks like this:
   *
   *   PHP message: Array
   *   (
   *       [0] => Array
   *           (
   *               [product_phase] => 14
   *               [product_name] => Needs to be removed
   *               [product_people] => 236
   *               [product_license] => 15
   *           )
   *
   *       [1] => Array
   *           (
   *               [product_phase] => 14
   *               [product_name] => Test 1
   *               [product_people] => 236
   *               [product_license] => 15
   *           )
   * 
   *       [2] => Array
   *           (
   *               [product_phase] => 14
   *               [product_name] => Test 2
   *               [product_people] => 236
   *               [product_license] => 15
   *           )
   *
   *       [3] => Array
   *           (
   *               [product_phase] => 14
   *               [product_name] => Test 3
   *               [product_people] => 236
   *               [product_license] => 15
   *           )
   *
   *   )
   * 
   * Each product has now its own array with the product_* as keys. 
   * The first product needs to be removed. The add product interface adds extra data even if the user has not pressed the add button
   * so we remove the first product using array_shift() and return the data. 
   *  
   *
   **/ 
  function cleanup_products( $product_keys = array() ) {
    //error_log( print_r( $product_keys, true ) ); 
    $the_products = array();
    if( sizeof( $product_keys ) > 0 ) { 
      foreach( $product_keys as $product_key => $product_values ) {
        foreach( $product_values as $product_nr => $value ) {
          $the_products[$product_nr][ $product_key ] = $value;
        }
      }
      if( sizeof( $the_products ) > 0 ) {
        array_shift( $the_products ); // remove the first set of data. This is not added by js but was part of the interface
        return $the_products;  
      } else {
        return new WP_Error( 'no_products', __( 'No products data found' ));
      }
    } else {
      return new WP_Error( 'data_malformed', __( ' Data was malformed' ));
    }
  }     

}
?>
