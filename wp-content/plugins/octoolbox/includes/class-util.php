<?php
if( ! class_exists( 'Util ' )) {
  class Util  {

    function __construct() {
    }


    // assumes we can only have one term selected! 
    public static function get_selected_term_id( $post_id, $taxonomy ) {
      return self::_get_selected_term( $post_id, $taxonomy, 'id'); 
    } 

    public static function get_selected_term_name( $post_id, $taxonomy ) {
      return self::_get_selected_term( $post_id, $taxonomy, 'name'); 
    } 

    public static function get_selected_term( $post_id, $taxonomy ) {
      return self::_get_selected_term( $post_id, $taxonomy, 'term_object' ); 
    }
  
    private static function _get_selected_term( $post_id, $taxonomy, $return = 'id' ) {
      $terms = get_the_terms( $post_id, $taxonomy); 
      if( is_array( $terms ) && sizeof( $terms ) > 0 ) {
        if( 'id' == $return ) { 
          return $terms[0]->term_id;
        } elseif( 'name' == $return ) {
          return $terms[0]->name; 
        } elseif( 'slug' == $return ) {
          return $terms[0]->slug; 
        } else {
          return $terms[0]; 
        }
      }
      return 0;
    } 


    public static function get_posts_by_term( $term, $taxonomy, $post_type ) {
      $args = array (
        'posts_per_page' => -1, 
        'post_status'    => 'any', 
        'post_type'      => $post_type,
        'tax_query'      => array( 
          array( 
            'taxonomy' => $taxonomy, 
            'field'    => 'term_id',
            'terms'    => $term
          )
        )
      );
      return get_posts( $args );
    }


    // retrieve posts and return as html options
    public static  function retrieve_posts_as_options( $post_type, $selected_id = null, $args = array() ) {
      $defaults = array( 
        'posts_per_page' => '-1',  
        'post_type'      => $post_type, 
      );

      $posts    = get_posts( $defaults ); 
      $html     ='';
      $selected = '';
      if( is_array( $posts ) && sizeof( $posts ) > 0 ) {
        foreach( $posts as $p) {
          if( $p->ID == $selected_id ) {
            $selected = "selected='selected'";
          } else {
            $selected = '';
          } 
          $html .= "<option value='$p->ID' $selected>";
          $html .= get_the_title( $p->ID );  
          $html .= "</option>";
        }
        return $html;
      }   
    }

    public static function retrieve_terms_as_options( $taxonomy, $selected_term_id = null, $args = array() ) {
      $defaults = array( 
        'taxonomy'   => $taxonomy, 
        'hide_empty' => false  
      );
    
      $params = array_merge( $defaults, $args );

      $terms    = get_terms( $params ); 
      $html     ='';
      $selected = '';
      if( is_array( $terms ) && sizeof( $terms ) > 0 ) {
        foreach( $terms as $t) {
          if( $t->term_id == $selected_term_id ) {
            $selected = "selected='selected'";
          } else {
            $selected = '';
          } 
          $html .= "<option value='$t->term_id' $selected>";
          $html .= $t->name;  
          $html .= "</option>";
        }
        return $html;
      }   
    }


    // removes multiple posts for ever...
    public static function remove_multiple_posts( $post_ids = array() ) {
      if( is_array( $post_ids ) && sizeof( $post_ids ) > 0 ) {
        foreach( $post_ids as $k => $post_id) {
          wp_delete_post( $post_id, true); // setting force_delete to true really deletes it..
        } 
      }
      return;
    }


    public static function get_license_url( $post, $seperator = ',') {
      $terms  = get_the_terms( $post, 'license' ); 
      $output = ''; 
      if( is_array( $terms ) && sizeof( $terms ) > 0 ) {
        foreach( $terms as $t ) {
          $license_url = get_term_meta( $t->term_id, 'license_url', true );
          if( empty( $license_url ) ) {
            $output .= $t->name . $seperator;
          } else {
            $output .= "<a href='$license_url'>" . $t->name . "</a>" . $seperator;
          }
        }
        $output = rtrim( $output, $seperator );
        return $output; 
      }
      return $output;
    }

    // return terms as a string seperated by a seperator
    public static function get_terms_string( $post, $taxonomy, $seperator = ', ') {
      $terms  = get_the_terms( $post, $taxonomy ); 
      $output = ''; 
      if( is_array( $terms ) && sizeof( $terms ) > 0 ) {
        foreach( $terms as $t ) {
          $output .= $t->name . $seperator; 
        }
        $output = rtrim( $output, $seperator );
        return $output; 
      }
      return $output;
    }
      

    // make it easier to add a hidden input field
    public static function add_hidden_field( $name, $value) {
      return "<input type='hidden' name='$name' value='$value'>"; 
    }

    
    // @todo need to check what happens if the title happens to be empty
    // what happens when the term already exists?
    public static function add_cpt_to_taxonomy( $id, $post, $tax, $is_update = false ) {
      // do nothing if revision or autosave
      if( wp_is_post_revision( $id ) ) { 
        return; 
      } 

      if( wp_is_post_autosave( $id ) ) {
        return; 
      } 

      $result = wp_set_object_terms( $id, $post->post_title, $tax, $is_update ); 
      if( is_wp_error( $result ) ){
      }
    }  

   
    public static function get_oct_option( $option_key, $default = null ) {
      $options = get_option( 'oct_settings' ); 
      if( is_array( $options ) && array_key_exists( $option_key, $options )) { 
        return $options[ $option_key ]; 
      }
      return $default;
    } 


    // retrieve metadata from an object. Assume all metadata is saved in an array
    // per Project, Client etc.
    // Requires an post_meta array name...
    public static function get_post_metadata_value( $post_metadata_name, $post_id, $key ) {
      $metadata = get_post_meta( $post_id, $post_metadata_name, false);   
      if( is_array( $metadata ) && sizeof( $metadata ) > 0 ) {
        foreach( $metadata as $data_array ) {
          if( array_key_exists( $key, $data_array ) ) {
            return $data_array[ $key ]; 
          } else {
            return null;
          }
        }
      } else {
        return new WP_Error( 'octoolbox', __( 'No metadata array found', 'YOUR-TEXTDOMAIN') );
      }
    }

  }
} else {
  error_log( ' Could not instantiate Utils class, due to the class already existing' ); 
}
?>
