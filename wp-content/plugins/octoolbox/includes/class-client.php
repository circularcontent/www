<?php 

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OCClient {

  function __construct() {
    add_action( 'save_post_oct-client', array( $this, 'save_metaboxes' ), 11 );
    add_action( 'edit_post_oct-client', array( $this, 'save_metaboxes' ), 11 );
  }

// create OCT client CPT
  function cpt_oct_client_init() {
    register_post_type( 'oct-client', array(
      'labels'            => array(
        'name'                => __( 'Clients', 'YOUR-TEXTDOMAIN' ),
        'singular_name'       => __( 'Client', 'YOUR-TEXTDOMAIN' ),
        'all_items'           => __( 'All Clients', 'YOUR-TEXTDOMAIN' ),
        'new_item'            => __( 'New client', 'YOUR-TEXTDOMAIN' ),
        'add_new'             => __( 'Add New', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'        => __( 'Add New client', 'YOUR-TEXTDOMAIN' ),
        'edit_item'           => __( 'Edit client', 'YOUR-TEXTDOMAIN' ),
        'view_item'           => __( 'View client', 'YOUR-TEXTDOMAIN' ),
        'search_items'        => __( 'Search clients', 'YOUR-TEXTDOMAIN' ),
        'not_found'           => __( 'No clients found', 'YOUR-TEXTDOMAIN' ),
        'not_found_in_trash'  => __( 'No clients found in trash', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'   => __( 'Parent client', 'YOUR-TEXTDOMAIN' ),
        'menu_name'           => __( 'Clients', 'YOUR-TEXTDOMAIN' ),
      ),
      'public'            => false,
      'hierarchical'      => false,
      'show_ui'           => true,
      'show_in_nav_menus' => false,
      'show_in_menu'      => 'edit.php?post_type=oct-project',
      'supports'          => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions' ),
      'register_meta_box_cb' => array( $this, 'cpt_oct_client_metaboxes'), 
      'has_archive'       => false,
      'rewrite'           => true,
      'query_var'         => true,
      'menu_icon'         => 'dashicons-admin-post',
      'show_in_rest'      => true,
      'rest_base'         => 'oct-client',
      'rest_controller_class' => 'WP_REST_Posts_Controller',
    ) );

  }

  // Setup messages for client 
  function cpt_oct_client_updated_messages( $messages ) {
    global $post;

    $permalink = get_permalink( $post );

    $messages['oct-client'] = array(
      0 => '', // Unused. Messages start at index 1.
      1 => sprintf( __('Client updated. <a target="_blank" href="%s">View client</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      2 => __('Custom field updated.', 'YOUR-TEXTDOMAIN'),
      3 => __('Custom field deleted.', 'YOUR-TEXTDOMAIN'),
      4 => __('Client updated.', 'YOUR-TEXTDOMAIN'),
      /* translators: %s: date and time of the revision */
      5 => isset($_GET['revision']) ? sprintf( __('Client restored to revision from %s', 'YOUR-TEXTDOMAIN'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
      6 => sprintf( __('Client published. <a href="%s">View client</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      7 => __('Client saved.', 'YOUR-TEXTDOMAIN'),
      8 => sprintf( __('Client submitted. <a target="_blank" href="%s">Preview client</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
      9 => sprintf( __('Client scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview client</a>', 'YOUR-TEXTDOMAIN'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
      10 => sprintf( __('Client draft updated. <a target="_blank" href="%s">Preview client</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
    );

    return $messages;
  }
  
  function cpt_oct_client_metaboxes( $post ) {
    add_meta_box( 'oct-contact-person', 'Contact person', array( $this, 'mbx_render_contact_person' ), null, 'advanced', 'high', null );  
  }

  function mbx_render_contact_person( $post ) {
    $contact_url = admin_url('/post-new.php?post_type=oct-person');
    
    $contact_id = Util::get_post_metadata_value( '_octoolbox_client_metadata', $post->ID, 'contact_id' );
    $contact_id = is_wp_error( $contact_id ) ? null : $contact_id; 


    $html = wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce', true, false );
    $html .= "<select name='_octoolbox_client_metadata[contact_id]' id='oct-client-contact-person'>";
    $html .= Util::retrieve_posts_as_options( 'oct-person', $contact_id ); 
    $html .= "</select>";
    $html .= "Or <a href='$contact_url'>add a new contact person</a>";
    echo $html; 
  }

  // TODO validation
  function save_metaboxes( $post_id ) {
    
    if ( ! isset( $_POST['octoolbox_nonce'] ) ) {
      error_log( 'nononce'); 
      return $post_id;
    } 

    if( ! check_admin_referer( 'octoolbox_nonce', 'octoolbox_nonce' ) ) {
      error_log( 'invalid nonce'); 
      return $post_id;
    }

    if( isset( $_POST[ '_octoolbox_client_metadata' ] ) ) {
      $data = $_POST[ '_octoolbox_client_metadata' ];
      update_post_meta( $post_id, '_octoolbox_client_metadata', $data );
    }
  }

}
?>
