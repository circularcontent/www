<?php 

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OCPerson {
  // create CPT person
  
  function __construct() {
    add_action( 'save_post_oct-person', array( $this, 'link_with_taxonomy' ), 10, 3);
    add_action( 'save_post_oct-person', array( $this, 'save_metaboxes' ), 11 );
    add_action( 'edit_post_oct-person', array( $this, 'save_metaboxes' ), 11 );
    add_action( 'save_post_oct-person', array( $this, 'maybe_remove_contracts' ), 11, 1 );
  }

  function cpt_oct_person_init() {
    register_post_type( 'oct-person', array(
      'labels'            => array(
        'name'                => __( 'Persons', 'YOUR-TEXTDOMAIN' ),
        'singular_name'       => __( 'Person', 'YOUR-TEXTDOMAIN' ),
        'all_items'           => __( 'All Persons', 'YOUR-TEXTDOMAIN' ),
        'new_item'            => __( 'New person', 'YOUR-TEXTDOMAIN' ),
        'add_new'             => __( 'Add New', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'        => __( 'Add New person', 'YOUR-TEXTDOMAIN' ),
        'edit_item'           => __( 'Edit person', 'YOUR-TEXTDOMAIN' ),
        'view_item'           => __( 'View person', 'YOUR-TEXTDOMAIN' ),
        'search_items'        => __( 'Search persons', 'YOUR-TEXTDOMAIN' ),
        'not_found'           => __( 'No persons found', 'YOUR-TEXTDOMAIN' ),
        'not_found_in_trash'  => __( 'No persons found in trash', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'   => __( 'Parent person', 'YOUR-TEXTDOMAIN' ),
        'menu_name'           => __( 'Persons', 'YOUR-TEXTDOMAIN' ),
      ),
      'public'            => false,
      'hierarchical'      => false,
      'show_ui'           => true,
      'show_in_nav_menus' => true,
      'show_in_menu'      => 'edit.php?post_type=oct-project',
      'supports'          => array( 'title', 'thumbnail',  'revisions', 'editor' ),
      'register_meta_box_cb' => array( $this, 'cpt_oct_person_metaboxes'), 
      'has_archive'       => true,
      'rewrite'           => true,
      'query_var'         => true,
      'menu_icon'         => 'dashicons-admin-post',
      'show_in_rest'      => true,
      'rest_base'         => 'oct-person',
      'rest_controller_class' => 'WP_REST_Posts_Controller',
    ) );
  }


  function ct_oct_person_product_init() {
    register_taxonomy( 'product', array( 'oct-person', 'oct-product', 'oct-contract'), array(
      'hierarchical'      => false,
      'public'            => false,
      'show_in_nav_menus' => true,
      'show_ui'           => false,
      'show_admin_column' => true,
      'query_var'         => false,
      'rewrite'           => true,
      'capabilities'      => array(
        'manage_terms'  => 'edit_posts',
        'edit_terms'    => 'edit_posts',
        'delete_terms'  => 'edit_posts',
        'assign_terms'  => 'edit_posts'
      ),
      'labels'            => array(
        'name'                       => __( 'Products', 'YOUR-TEXTDOMAIN' ),
        'singular_name'              => _x( 'product', 'taxonomy general name', 'YOUR-TEXTDOMAIN' ),
        'search_items'               => __( 'Search products', 'YOUR-TEXTDOMAIN' ),
        'popular_items'              => __( 'Popular products', 'YOUR-TEXTDOMAIN' ),
        'all_items'                  => __( 'All products', 'YOUR-TEXTDOMAIN' ),
        'parent_item'                => __( 'Parent product', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'          => __( 'Parent product:', 'YOUR-TEXTDOMAIN' ),
        'edit_item'                  => __( 'Edit product', 'YOUR-TEXTDOMAIN' ),
        'update_item'                => __( 'Update product', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'               => __( 'New product', 'YOUR-TEXTDOMAIN' ),
        'new_item_name'              => __( 'New product', 'YOUR-TEXTDOMAIN' ),
        'separate_items_with_commas' => __( 'products separated by comma', 'YOUR-TEXTDOMAIN' ),
        'add_or_remove_items'        => __( 'Add or remove products', 'YOUR-TEXTDOMAIN' ),
        'choose_from_most_used'      => __( 'Choose from the most used products', 'YOUR-TEXTDOMAIN' ),
        'not_found'                  => __( 'No products found.', 'YOUR-TEXTDOMAIN' ),
        'menu_name'                  => __( 'products', 'YOUR-TEXTDOMAIN' ),
      ),
      'show_in_rest'      => true,
      'rest_base'         => 'product',
      'rest_controller_class' => 'WP_REST_Terms_Controller',
    ) );
  }


  // link current CPT to taxonomy
  function link_with_taxonomy( $id, $post, $is_update ) {
    Util::add_cpt_to_taxonomy( $id, $post, 'person', false); 
  }


  function cpt_oct_person_updated_messages( $messages ) {
    global $post;

    $permalink = get_permalink( $post );

    $messages['oct-person'] = array(
      0 => '', // Unused. Messages start at index 1.
      1 => sprintf( __('Person updated. <a target="_blank" href="%s">View person</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      2 => __('Custom field updated.', 'YOUR-TEXTDOMAIN'),
      3 => __('Custom field deleted.', 'YOUR-TEXTDOMAIN'),
      4 => __('Person updated.', 'YOUR-TEXTDOMAIN'),
      /* translators: %s: date and time of the revision */
      5 => isset($_GET['revision']) ? sprintf( __('Person restored to revision from %s', 'YOUR-TEXTDOMAIN'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
      6 => sprintf( __('Person published. <a href="%s">View person</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      7 => __('Person saved.', 'YOUR-TEXTDOMAIN'),
      8 => sprintf( __('Person submitted. <a target="_blank" href="%s">Preview person</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
      9 => sprintf( __('Person scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview person</a>', 'YOUR-TEXTDOMAIN'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
      10 => sprintf( __('Person draft updated. <a target="_blank" href="%s">Preview person</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true', 
      $permalink ) ) ),
    );

    return $messages;
  }
  
  function cpt_oct_person_metaboxes( $post ) {
    add_meta_box( 'oct-person-basics', 'Contact information', array( $this, 'mbx_render_person_fields' ), null, 'advanced', 'high', null );  
    add_meta_box( 'oct-person-contract', 'Contracts', array( $this, 'mbx_render_person_contract' ), null, 'advanced', 'high', null );  
  }
  
  
  function mbx_render_person_contract( $post ) {
    $selected_contract = Util::get_selected_term_id( $post->ID, 'product');

    $html = '';
    $html .= wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce', true, false );
    $html .= "<p>";
    $html .= "<table class='widefat wp-list-table striped' id='octoolbox_person_contracts'>";
    $html .= '<thead>';
    $html .= "<tr>";
    $html .="<th><strong>Product title</strong></th>";
    $html .="<th><strong>License</strong></th>";
    $html .="<th><strong>Status</strong></th>";
    $html .="<th colspan='2'><strong>Actions</strong></strong></th>";
    $html .="</tr>";
    $html .= '</thead>';

    $products = $this->get_products( $post );
    if( (is_array( $products) && sizeof( $products ) > 0 ) ) { 
      $html .= '<tbody>';
      foreach( $products as $key =>  $p ) {
        $html .= "<tr>";
        $html .="<td>" . get_the_title( $p ) . "</td>";
        $html .="<td>" . Util::get_terms_string( $p, 'license') . "</td>";
        $html .="<td>" . get_post_status( $p->ID ). "</td>";
        $contract_id = $this->get_contract( $post->ID, $p->ID); 
        if( false === $contract_id ) {
          $html .="<td><input type='checkbox' value='$p->ID' name='_octoolbox_person_add_contracts[]'>Generate contract</td>";
          $html .="<td></td>";
        } else {
          $edit_url = admin_url("/post.php?post=$contract_id&action=edit"); 
          $html .="<td><a href='$edit_url'>Edit contract</a></td>";
          $html .="<td><input type='checkbox' value='$contract_id' name='_octoolbox_person_remove_contracts[]'>Remove</td>";
        }
        $html .= Util::add_hidden_field('_octoolbox_person_existing_products[]', $p->ID ); 
        $html .= "</td>";
        $html .="</tr>";
      }   
      $html .= '</tbody>';
    } 
    
    $html .= "</table>"; 
    $html .="</p>";
    echo $html;  
  }



  function mbx_render_person_fields( $post ) {
    $fields = array(
      array( 'field_label' => 'Telephone number', 'field_id' => 'telnr', 'field_value' => ''),  
      array( 'field_label' => 'Mobile number',    'field_id' => 'mobnr', 'field_value' => ''),  
      array( 'field_label' => 'E-mail',           'field_id' => 'email', 'field_value' => ''),  
    );

    $html = wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce', true, false );

    foreach( $fields as $f ) {
      $name = "_octoolbox_person_metadata[{$f['field_id']}]";
      $id   = 'oct-person-' . $f['field_id']; 
      $val  = Util::get_post_metadata_value( '_octoolbox_person_metadata', $post->ID, $f['field_id'] );
      $val  = is_wp_error( $val ) ? '' : $val; 

      $html .= "<label>" . $f['field_label'] . ":</label><br>";  
      $html .= "<input name='$name' id='$id' type='text' value='$val'/><br>";
    }
    echo $html;
  }




  // TODO validation
  function save_metaboxes( $post_id ) {
    
    if ( ! isset( $_POST['octoolbox_nonce'] ) ) {
      return $post_id;
    } 

    if( ! check_admin_referer( 'octoolbox_nonce', 'octoolbox_nonce' ) ) {
      return $post_id;
    }

    if( isset( $_POST[ '_octoolbox_person_metadata' ] ) ) {
      $data = $_POST[ '_octoolbox_person_metadata' ];
      update_post_meta( $post_id, '_octoolbox_person_metadata', $data );
    }

    if( isset( $_POST['_octoolbox_person_add_contracts'] ) ) {
      $product_ids = $_POST[ '_octoolbox_person_add_contracts' ];
      $this->generate_contracts( $product_ids, $post_id );   
    }
  }

  function maybe_remove_contracts( $post_id ) {
    if( isset( $_POST['_octoolbox_person_remove_contracts'] ) ) {
      $remove_contract_ids = $_POST['_octoolbox_person_remove_contracts'];
      Util::remove_multiple_posts( $remove_contract_ids ); 
      return;
    }
  } 
  
  // use a person_id and product_id to see if there's a contract if not return 
  // false
  function get_contract( $person_id, $product_id ) {
    $product_term_id = Util::get_selected_term_id( $product_id, 'product' ); 
    $person_term_id  = Util::get_selected_term_id( $person_id, 'person');     
    $params = array(
      'post_type' => 'oct-contract',
      'tax_query' => array( 
        'relation' => 'AND',
        array( 
          'taxonomy' => 'product',
          'field'    => 'term_id',
          'terms'    => $product_term_id 
        ),
        array( 
          'taxonomy' => 'person',
          'field'    => 'term_id',
          'terms'    => $person_term_id 
        )
      )); 
    $posts = get_posts( $params );
    if( is_array( $posts ) && sizeof( $posts ) > 0 ) {
      return $posts[0]->ID; 
    }
    return false; 
  }



  function generate_contracts( $product_ids, $person_id ) {
    if( is_array( $product_ids ) && sizeof( $product_ids) > 0 ) {
      $contract_post_data = array( 
        'post_type'   => 'oct-contract',
        'post_status' => 'publish',
      ); 
      foreach( $product_ids as $product_id ) {
        $contract_text = $this->generate_contract_text( $product_id, $person_id);  
        $contract_post_data[ 'post_content' ] = $contract_text;
        $contract_post_data[ 'post_title' ]   = sprintf( __('%1$s\'s contract for: %2$s  '), get_the_title( $person_id ), get_the_title($product_id));
        $product_term = Util::get_selected_term_name( $product_id, 'product' ); 
        $person_term  = Util::get_selected_term_name( $person_id, 'person');     
        $contract_post_data[ 'tax_input' ] = array( 'product' => $product_term, 'person' => $person_term );  
        wp_insert_post( $contract_post_data ); 
      }      
    }
  }


  // TODO fix generating the text
  function generate_contract_text( $product_id, $person_id ) {
    $contract_text = Util::get_oct_option( 'contract_text' ); 
    // #TODO need to refactor this into the main class
    $contract_keywords = array( 
      '[person_name]', 
      '[project_title]',
      '[client_name]', 
      '[product_title]', 
      '[license_name]'
    ); 

    foreach( $contract_keywords as $keyword ) {
      switch( $keyword ) {
        case '[person_name]': 
          $contract_keywords_values[ $keyword ] = get_the_title( $person_id );
        break;
        case '[product_title]': 
          $contract_keywords_values[ $keyword ] = get_the_title( $product_id );
        break;
        case '[client_name]': 
          $contract_keywords_values[ $keyword ] = $this->get_the_client( $product_id );
        break;
        case '[project_title]': 
          $contract_keywords_values[ $keyword ] = $this->get_the_project( $product_id );
        break;
        case '[license_name]': 
          $contract_keywords_values[ $keyword ] = $this->get_the_license( $product_id );
        break;
      }
    } 

    foreach( $contract_keywords_values as $keyword => $keyword_replacement) {
      $contract_text = str_ireplace( $keyword, $keyword_replacement, $contract_text); 
    }
    return $contract_text;    
  }


  function get_the_license( $product_id ) {
    return Util::get_selected_term_name( $product_id, 'license'); 
  }

  function get_the_project( $product_id ) {
    return Util::get_selected_term_name( $product_id, 'project' );
  }

  function get_the_client( $product_id ) {
    $term_id = Util::get_selected_term_id( $product_id, 'project' );
    error_log( print_r( $term_id, true )); 
    $params = array( 
      'post_type'   => 'oct-project',
      'post_status' => 'any', 
      'tax_query' => array( 
        array(
          'taxonomy' => 'project',
          'field'    => 'term_id',
          'terms'    => $term_id   
        )
      ) 
    );
    $posts = get_posts( $params );  
    error_log( print_r( $posts, true ) ); 
    if( is_array( $posts ) && sizeof( $posts ) > 0 ) {
      $post_id = $posts[0]->ID;
      $client_id = Util::get_post_metadata_value( '_octoolbox_project_metadata', $post_id, 'client_id' );
      return get_the_title( $client_id ); 
    }
  }


  // retrieve the products belonging to this project
  // TODO: methods like this should be made available in an easy way for themers
  function get_products( $post ) {
    $terms = get_the_terms( $post->ID, 'product'); 
    //error_log( print_r( $terms, true ) ); 
    if( is_array( $terms ) && sizeof( $terms ) > 0 ) {
      $terms_ids = array();
      foreach( $terms as $term ) {
        $term_ids[] = $term->term_id;
      }
      //error_log( print_r( $term_ids, true ) ); 
      $params = array( 
        'posts_per_page' => '-1',
        'post_status'    => 'any',  
        'post_type'      => 'oct-product',
        'tax_query'      => array(
          array( 
            'taxonomy' => 'product',
            'field'    => 'term_id',
            'terms'    => $term_ids
          )
        )
      );
      $products = get_posts( $params ); 
      //error_log( print_r( $products, true ) ); 
      return $products; 
    }
  }

  function _as_options( $posts, $selected_id = null ) {
    $html ='';
    if ( is_array( $posts ) && sizeof($posts) ) {
      foreach( $posts as $p) {
        if( $p->ID == $selected_id ) {
          $selected = "selected='selected'";
        } else {
          $selected = '';
        } 
        $html .= "<option value='$p->ID' $selected>";
        $html .= get_the_title( $p->ID );  
        $html .= "</option>";
      }
    }   
    return $html;
  }



}
?>
