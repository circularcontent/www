<?php 

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OCContract {
  // create CPT person
  
  function __construct() {
  }

  function cpt_oct_contract_init() {
    register_post_type( 'oct-contract', array(
      'labels'            => array(
        'name'                => __( 'Contracts', 'YOUR-TEXTDOMAIN' ),
        'singular_name'       => __( 'Contract', 'YOUR-TEXTDOMAIN' ),
        'all_items'           => __( 'All Contracts', 'YOUR-TEXTDOMAIN' ),
        'new_item'            => __( 'New Contract', 'YOUR-TEXTDOMAIN' ),
        'add_new'             => __( 'Add New', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'        => __( 'Add New contract', 'YOUR-TEXTDOMAIN' ),
        'edit_item'           => __( 'Edit Contract', 'YOUR-TEXTDOMAIN' ),
        'view_item'           => __( 'View Contract', 'YOUR-TEXTDOMAIN' ),
        'search_items'        => __( 'Search Contracts', 'YOUR-TEXTDOMAIN' ),
        'not_found'           => __( 'No contracts found', 'YOUR-TEXTDOMAIN' ),
        'not_found_in_trash'  => __( 'No contracts found in trash', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'   => __( 'Parent contract', 'YOUR-TEXTDOMAIN' ),
        'menu_name'           => __( 'Contracts', 'YOUR-TEXTDOMAIN' ),
      ),
      'public'            => false,
      'hierarchical'      => false,
      'show_ui'           => true,
      'show_in_nav_menus' => true,
      'show_in_menu'      => 'edit.php?post_type=oct-project', // need to set to false before release
      'supports'          => array( 'title', 'editor', 'revisions' ),
      'has_archive'       => false,
      'rewrite'           => true,
      'query_var'         => true,
      'menu_icon'         => 'dashicons-admin-post',
      'show_in_rest'      => false,
      'rest_base'         => 'oct-contract',
      'rest_controller_class' => 'WP_REST_Posts_Controller',
    ) );
  }

  function ct_oct_contract_person_init() {
    register_taxonomy( 'person', array( 'oct-contract', 'oct-person' ), array(
      'hierarchical'      => false,
      'public'            => false,
      'show_in_nav_menus' => true,
      'show_ui'           => false,
      'show_admin_column' => true,
      'query_var'         => false,
      'rewrite'           => true,
      'capabilities'      => array(
        'manage_terms'  => 'edit_posts',
        'edit_terms'    => 'edit_posts',
        'delete_terms'  => 'edit_posts',
        'assign_terms'  => 'edit_posts'
      ),
      'labels'            => array(
        'name'                       => __( 'Person', 'YOUR-TEXTDOMAIN' ),
        'singular_name'              => _x( 'Person', 'taxonomy general name', 'YOUR-TEXTDOMAIN' ),
        'search_items'               => __( 'Search people', 'YOUR-TEXTDOMAIN' ),
        'popular_items'              => __( 'Popular people', 'YOUR-TEXTDOMAIN' ),
        'all_items'                  => __( 'All people', 'YOUR-TEXTDOMAIN' ),
        'parent_item'                => __( 'Parent person', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'          => __( 'Parent person:', 'YOUR-TEXTDOMAIN' ),
        'edit_item'                  => __( 'Edit person', 'YOUR-TEXTDOMAIN' ),
        'update_item'                => __( 'Update person', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'               => __( 'New person', 'YOUR-TEXTDOMAIN' ),
        'new_item_name'              => __( 'New person', 'YOUR-TEXTDOMAIN' ),
        'separate_items_with_commas' => __( 'persons separated by comma', 'YOUR-TEXTDOMAIN' ),
        'add_or_remove_items'        => __( 'Add or remove persons', 'YOUR-TEXTDOMAIN' ),
        'choose_from_most_used'      => __( 'Choose from the most used persons', 'YOUR-TEXTDOMAIN' ),
        'not_found'                  => __( 'No persons found.', 'YOUR-TEXTDOMAIN' ),
        'menu_name'                  => __( 'persons', 'YOUR-TEXTDOMAIN' ),
      ),
      'show_in_rest'      => false,
      'rest_base'         => 'person',
      'rest_controller_class' => 'WP_REST_Terms_Controller',
    ) );
  }

  function cpt_oct_contract_updated_messages( $messages ) {
    global $post;

    $permalink = get_permalink( $post );

    $messages['oct-contract'] = array(
      0 => '', // Unused. Messages start at index 1.
      1 => sprintf( __('Contract updated. <a target="_blank" href="%s">View contract</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      2 => __('Custom field updated.', 'YOUR-TEXTDOMAIN'),
      3 => __('Custom field deleted.', 'YOUR-TEXTDOMAIN'),
      4 => __('Contract updated.', 'YOUR-TEXTDOMAIN'),
      /* translators: %s: date and time of the revision */
      5 => isset($_GET['revision']) ? sprintf( __('Contract restored to revision from %s', 'YOUR-TEXTDOMAIN'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
      6 => sprintf( __('Contract published. <a href="%s">View contract</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      7 => __('Contract saved.', 'YOUR-TEXTDOMAIN'),
      8 => sprintf( __('Contract submitted. <a target="_blank" href="%s">Preview contract</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
      9 => sprintf( __('Contract scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview contract</a>', 'YOUR-TEXTDOMAIN'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
      10 => sprintf( __('Contract draft updated. <a target="_blank" href="%s">Preview contract</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true', 
      $permalink ) ) ),
    );

    return $messages;
  }
  
  
  
}
?>
