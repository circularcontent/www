<?php 

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OCProduct {

  function __construct() {
    
    add_action( 'save_post_oct-product', array( $this, 'link_product_with_team' ), 10, 3);
    
    add_action( 'save_post_oct-product', array( $this, 'save_metaboxes' ), 11 );
    add_action( 'edit_post_oct-product', array( $this, 'save_metaboxes' ), 11 );
    
    add_action( 'save_post_oct-product', array( $this, 'save_team' ), 11 );
    add_action( 'edit_post_oct-product', array( $this, 'save_team' ), 11 );

    
    add_action( 'save_post_oct-product', array( $this, 'maybe_update_people_from_product' ), 11, 1 );
    add_action( 'save_post_oct-product', array( $this, 'maybe_remove_people_from_product' ), 11, 1 );

    // add a license url to the license taxonomy
    add_action( 'license_add_form_fields', array( $this, 'new_license_url' ), 10, 1 );
    add_action( 'created_license', array( $this, 'save_license_url' ), 10, 2 );
    add_action( 'edited_license', array( $this, 'save_license_url' ), 10, 2 );
    add_action( 'license_edit_form_fields', array( $this, 'edit_license_url' ), 10, 2 );
    add_filter( 'manage_edit-license_columns', array( $this, 'add_license_url_column') ); 
    add_filter( 'manage_license_custom_column', array( $this, 'add_license_url_column_content'), 10,3 ); 

  }

  // create CPT product
  function cpt_oct_product_init() {
    register_post_type( 'oct-product', array(
      'labels'            => array(
        'name'                => __( 'Products', 'YOUR-TEXTDOMAIN' ),
        'singular_name'       => __( 'Product', 'YOUR-TEXTDOMAIN' ),
        'all_items'           => __( 'All Products', 'YOUR-TEXTDOMAIN' ),
        'new_item'            => __( 'New product', 'YOUR-TEXTDOMAIN' ),
        'add_new'             => __( 'Add New', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'        => __( 'Add New product', 'YOUR-TEXTDOMAIN' ),
        'edit_item'           => __( 'Edit product', 'YOUR-TEXTDOMAIN' ),
        'view_item'           => __( 'View product', 'YOUR-TEXTDOMAIN' ),
        'search_items'        => __( 'Search products', 'YOUR-TEXTDOMAIN' ),
        'not_found'           => __( 'No products found', 'YOUR-TEXTDOMAIN' ),
        'not_found_in_trash'  => __( 'No products found in trash', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'   => __( 'Parent product', 'YOUR-TEXTDOMAIN' ),
        'menu_name'           => __( 'Products', 'YOUR-TEXTDOMAIN' ),
      ),
      'public'            => true,
      'hierarchical'      => false,
      'show_ui'           => true,
      'show_in_nav_menus' => false,
      'show_in_menu'      => 'edit.php?post_type=oct-project',
      'supports'          => array( 'title', 'editor', 'author', 'revisions', 'thumbnail' ),
      'register_meta_box_cb' => array( $this, 'metaboxes'), 
      'has_archive'       => true,
      'rewrite'           => true,
      'query_var'         => true,
      'menu_icon'         => 'dashicons-admin-post',
      'show_in_rest'      => true,
      'rest_base'         => 'oct-product',
      'rest_controller_class' => 'WP_REST_Posts_Controller',
    ) );
  }
  
  // the license of the  product 
  function ct_oct_product_license_init() {
    register_taxonomy( 'license', array( 'oct-product' ), array(
      'hierarchical'      => true,
      'public'            => true,
      'show_in_nav_menus' => false,
      'show_ui'           => true,
      'show_admin_column' => true,
      'query_var'         => true,
      'rewrite'           => true,
      'meta_box_cb'       => false, // hide metabox
      'capabilities'      => array(
        'manage_terms'  => 'edit_posts',
        'edit_terms'    => 'edit_posts',
        'delete_terms'  => 'edit_posts',
        'assign_terms'  => 'edit_posts'
      ),
      'labels'            => array(
        'name'                       => __( 'License', 'YOUR-TEXTDOMAIN' ),
        'singular_name'              => _x( 'License', 'taxonomy general name', 'YOUR-TEXTDOMAIN' ),
        'search_items'               => __( 'Search licenses', 'YOUR-TEXTDOMAIN' ),
        'popular_items'              => __( 'Popular licenses', 'YOUR-TEXTDOMAIN' ),
        'all_items'                  => __( 'All licenses', 'YOUR-TEXTDOMAIN' ),
        'parent_item'                => __( 'Parent license', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'          => __( 'Parent license:', 'YOUR-TEXTDOMAIN' ),
        'edit_item'                  => __( 'Edit license', 'YOUR-TEXTDOMAIN' ),
        'update_item'                => __( 'Update license', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'               => __( 'New license', 'YOUR-TEXTDOMAIN' ),
        'new_item_name'              => __( 'New license', 'YOUR-TEXTDOMAIN' ),
        'separate_items_with_commas' => __( 'Licenses separated by comma', 'YOUR-TEXTDOMAIN' ),
        'add_or_remove_items'        => __( 'Add or remove licenses', 'YOUR-TEXTDOMAIN' ),
        'choose_from_most_used'      => __( 'Choose from the most used licenses', 'YOUR-TEXTDOMAIN' ),
        'not_found'                  => __( 'No licenses found.', 'YOUR-TEXTDOMAIN' ),
        'menu_name'                  => __( 'Licenses', 'YOUR-TEXTDOMAIN' ),
      ),
      'show_in_rest'      => true,
      'rest_base'         => 'license',
      'rest_controller_class' => 'WP_REST_Terms_Controller',
    ) );
  }

  // project phase the product is in 
  function ct_oct_product_phase_init() {
    register_taxonomy( 'phase', array( 'oct-product' ), array(
      'hierarchical'      => true,
      'public'            => true,
      'show_in_nav_menus' => false,
      'show_ui'           => true,
      'show_admin_column' => true,
      'query_var'         => true,
      'rewrite'           => true,
      'meta_box_cb'       => false, // hide metabox
      'capabilities'      => array(
        'manage_terms'  => 'edit_posts',
        'edit_terms'    => 'edit_posts',
        'delete_terms'  => 'edit_posts',
        'assign_terms'  => 'edit_posts'
      ),
      'labels'            => array(
        'name'                       => __( 'Project phase', 'YOUR-TEXTDOMAIN' ),
        'singular_name'              => _x( 'Phase', 'taxonomy general name', 'YOUR-TEXTDOMAIN' ),
        'search_items'               => __( 'Search phases', 'YOUR-TEXTDOMAIN' ),
        'popular_items'              => __( 'Popular project phases', 'YOUR-TEXTDOMAIN' ),
        'all_items'                  => __( 'All phases', 'YOUR-TEXTDOMAIN' ),
        'parent_item'                => __( 'Parent phase', 'YOUR-TEXTDOMAIN' ),
        'parent_item_colon'          => __( 'Parent phase:', 'YOUR-TEXTDOMAIN' ),
        'edit_item'                  => __( 'Edit phase', 'YOUR-TEXTDOMAIN' ),
        'update_item'                => __( 'Update phase', 'YOUR-TEXTDOMAIN' ),
        'add_new_item'               => __( 'New phase', 'YOUR-TEXTDOMAIN' ),
        'new_item_name'              => __( 'New phase', 'YOUR-TEXTDOMAIN' ),
        'separate_items_with_commas' => __( 'Phases separated by comma', 'YOUR-TEXTDOMAIN' ),
        'add_or_remove_items'        => __( 'Add or remove phases', 'YOUR-TEXTDOMAIN' ),
        'choose_from_most_used'      => __( 'Choose from the most used phases', 'YOUR-TEXTDOMAIN' ),
        'not_found'                  => __( 'No phases found.', 'YOUR-TEXTDOMAIN' ),
        'menu_name'                  => __( 'Phases', 'YOUR-TEXTDOMAIN' ),
      ),
      'show_in_rest'      => true,
      'rest_base'         => 'phase',
      'rest_controller_class' => 'WP_REST_Terms_Controller',
    ) );
  }
  // setup message for OCT products
  function cpt_oct_product_updated_messages( $messages ) {
    global $post;

    $permalink = get_permalink( $post );

    $messages['oct-product'] = array(
      0 => '', // Unused. Messages start at index 1.
      1 => sprintf( __('Product updated. <a target="_blank" href="%s">View product</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      2 => __('Custom field updated.', 'YOUR-TEXTDOMAIN'),
      3 => __('Custom field deleted.', 'YOUR-TEXTDOMAIN'),
      4 => __('Product updated.', 'YOUR-TEXTDOMAIN'),
      /* translators: %s: date and time of the revision */
      5 => isset($_GET['revision']) ? sprintf( __('Product restored to revision from %s', 'YOUR-TEXTDOMAIN'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
      6 => sprintf( __('Product published. <a href="%s">View product</a>', 'YOUR-TEXTDOMAIN'), esc_url( $permalink ) ),
      7 => __('Product saved.', 'YOUR-TEXTDOMAIN'),
      8 => sprintf( __('Product submitted. <a target="_blank" href="%s">Preview product</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
      9 => sprintf( __('Product scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview product</a>', 'YOUR-TEXTDOMAIN'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
      10 => sprintf( __('Product draft updated. <a target="_blank" href="%s">Preview product</a>', 'YOUR-TEXTDOMAIN'), esc_url( add_query_arg( 'preview', 'true'
      , $permalink ) ) ),
    );

    return $messages;
  }


  function link_product_with_team( $id, $post) {
    $this->link_with_product_taxonomy( $id, $post, false); // update needs to be false to prevent a product linked to multiple teams
  }

  function link_with_product_taxonomy( $id, $post, $is_update = false ) {
    Util::add_cpt_to_taxonomy( $id, $post, 'product', $is_update ); 
  }


  function metaboxes( $post ) {
    add_meta_box( 'oct-team-member', 'Team', array( $this, 'mbx_render_team_member' ), null, 'advanced', 'high', null );  
    add_meta_box( 'oct-license', 'License', array( $this, 'mbx_render_license' ), null, 'side', 'default', null );  
    add_meta_box( 'oct-project-phase', 'Project phase', array( $this, 'mbx_render_project_phase' ), null, 'side', 'default', null );  
    add_meta_box( 'oct-project', 'Project', array( $this, 'mbx_render_project' ), null, 'side', 'default', null );  
    add_meta_box( 'oct-uri', 'Download URL', array( $this, 'mbx_render_download' ), null, 'side', 'default', null );  
  }

  function mbx_render_team_member( $post ) {
    $contact_url = admin_url('/post-new.php?post_type=oct-person');
    $team = $this->get_people( $post ); 

    $html = "<p>";
    $html .= "<table class='widefat wp-list-table striped' id='octoolbox_product_team'>";
    $html .= '<thead>';
    $html .= "<tr>";
    $html .="<th><strong>Name</strong></th>";
    $html .="<th><strong>Status</strong></th>";
    $html .="<th colspan='2'><strong>Actions</strong></strong></th>";
    $html .="</tr>";
    $html .= '</thead>';

    if( (is_array( $team) && sizeof( $team ) > 0 ) ) { 
      $html .= '<tbody>';
      foreach( $team as $key =>  $m ) {
        $edit_url = admin_url("/post.php?post=$m->ID&action=edit"); 
        
        $html .= "<tr>";
        $html .="<td>" . get_the_title( $m ) . "</td>";
        $html .="<td>" . get_post_status( $m->ID ). "</td>";
        $html .="<td><a href='$edit_url' title='Edit person'>Edit</a>";
        $html .= Util::add_hidden_field('_octoolbox_product_existing_team_members[]', $m->ID ); 
        $html .= "</td>"; 
        $html .="<td><input type='checkbox' value='$m->ID' name='_octoolbox_product_remove_team_members[]'>Remove</td>";
        $html .="</tr>";
      }   
      $html .= '</tbody>';
    } 

    $html .= '<tfoot>';
    $html .= "<tr>";
    $html .= "<td>";
    
    $html .= "<select name='_octoolbox_product_people' id='oct-product-team-member'>";
    $html .= Util::retrieve_posts_as_options( 'oct-person', 0 ); 
    $html .= "</select> ";
    $html .= "<input type='button' name='_octoolbox_product_add_person' id='octoolbox_project_add_person' value='Add person' class='button'></input>";
    $html .= "</td>";
    $html .= "<td></td>";
    $html .= "<td></td>";
    $html .= "<td colspan='2'></td>";
    $html .= "<tr>";
    $html .= '</tfoot>';
    $html .= "</table>"; 
    $html .="</p>";
    
    $html .="<p>";
    $html .= __("Easily add new team members to your project. Do not forget to save your project to save your team. Use the project's edit button to add the rest of the necessary details");   
    $html .="</p>";
    echo $html;
  }


  function get_people( $post ) {
    $term_id = Util::get_selected_term_id( $post->ID, 'product' );   
    $defaults = array( 
      'posts_per_page' => '-1',
      'post_status'    => 'any',  
      'post_type'      => 'oct-person',
      'tax_query'      => array(
        array( 
          'taxonomy' => 'product',
          'field'    => 'term_id',
          'terms'    => $term_id
        )
      )
    );
    $products = get_posts( $defaults ); 
    return $products; 
  } 


  // remove the relation between people and the product, but doesn't remove 
  // people from the db
  function maybe_remove_people_from_product( $post_id ) {
    if( isset( $_POST['_octoolbox_product_remove_team_members'] ) ) {
      $remove_people_ids = $_POST['_octoolbox_product_remove_team_members'];
      $term_id = Util::get_selected_term_id( $post_id, 'product' );   
      if( is_array( $remove_people_ids ) && sizeof( $remove_people_ids ) > 0 ) {
        foreach( $remove_people_ids as $k => $person_id) {
          wp_remove_object_terms( $person_id, $term_id, 'product' ); 
        } 
      }
      return;
    }
  } 

  // updates existing people to use the most recent term_id
  function maybe_update_people_from_product( $post_id ) {
    if( isset( $_POST['_octoolbox_product_existing_team_members'] ) ) {
      $existing_people_ids = $_POST['_octoolbox_product_existing_team_members'];
      $remove_people_ids   = isset( $_POST['_octoolbox_product_remove_team_members'] )  ? $_POST['_octoolbox_product_remove_team_members'] : array(); 
      $people_ids = array_diff( $existing_people_ids, $remove_people_ids ); // only get the ids which are not supposed to be removed  
      if( is_array( $people_ids ) && sizeof( $people_ids ) > 0 ) {
        $product_term_id = Util::get_selected_term_id( $post_id, 'product' );
        foreach( $people_ids as $k => $person_id) {
          $result = wp_set_object_terms( $person_id, $product_term_id, 'product', false ); 
        } 
      }
      return;
    }
  }



  // TODO: keep in mind the metadata for a license and projectfase are 
  // taxonomies
  function mbx_render_license( $post ) {
    $selected_license = Util::get_selected_term_id( $post->ID, 'license');
    $html = '';
    $html .= wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce', true, false );
    $html .= "<select name='_octoolbox_product_metadata[license]' id='oct-product-license'>";
    $html .= Util::retrieve_terms_as_options( 'license', $selected_license ); 
    $html .= "</select>";
    echo $html;  
  }

  function mbx_render_project_phase( $post ) {
    $selected_phase = Util::get_selected_term_id( $post->ID, 'phase');
    $html = '';
    $html .= wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce', true, false );
    $html .= "<select name='_octoolbox_product_metadata[project_phase]' id='oct-product-phase'>";
    $html .= Util::retrieve_terms_as_options( 'phase', $selected_phase ); 
    $html .= "</select>";
    echo $html;  
  }
  
  function mbx_render_download( $post ) {
    $download_url = Util::get_post_metadata_value( '_octoolbox_product_metadata', $post->ID, 'download_url' );
    $download_url = is_wp_error( $download_url) ? '' : $download_url; 
    
    $html = wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce', true, false );
    $html .= "<input type='text' name='_octoolbox_product_metadata[download_url]' placeholder='https://...' value='$download_url'/>";
    echo $html; 
  }

  function mbx_render_project( $post ) {
    $selected_project = Util::get_selected_term_id( $post->ID, 'project');
    $projects = Util::get_posts_by_term( $selected_project, 'project', 'oct-project'); 
    $edit_link = ''; 
    if( is_array( $projects ) && sizeof( $projects ) > 0 ) {
      $pid = $projects[0]->ID; // first and only wordpress post id that we're interested in from an array of WP Post objects
      $edit_url  = admin_url("/post.php?post=$pid&action=edit"); 
      $edit_link = "<p><a href='$edit_url'>Edit currently selected project</a></p>"; 
    }
    
    
    $html = '';
    $html .= wp_nonce_field( 'octoolbox_nonce', 'octoolbox_nonce', true, false );
    $html .= "<select name='_octoolbox_product_metadata[project]' id='oct-product-project'>";
    $html .= Util::retrieve_terms_as_options( 'project', $selected_project ); 
    $html .= "</select>";
    $html .= $edit_link;
    echo $html;  
  }

  // @todo add proper validation & sanitation!
  function save_team( $post_id ) {
    if( isset( $_POST['existing_people_ids'] ) && is_array( $_POST['existing_people_ids'] ) ) {
      $existing_people_ids =  $_POST['existing_people_ids']; 
      foreach( $existing_people_ids as $person_id ) {
        $this->link_with_product_taxonomy( $person_id, get_post( $post_id ), true );
      } 
    }
  }




  // @todo add proper validation & sanitation!
  function save_metaboxes( $post_id ) {
    
    if ( ! isset( $_POST['octoolbox_nonce'] ) ) {
      return $post_id;
    } 

    if( ! check_admin_referer( 'octoolbox_nonce', 'octoolbox_nonce' ) ) {
      return $post_id;
    }

    if( isset( $_POST[ '_octoolbox_product_metadata' ] ) && is_array( $_POST[ '_octoolbox_product_metadata' ] ) ) {
      $metadata = $_POST[ '_octoolbox_product_metadata' ];
      foreach( $metadata as $key => $value ) {
        switch( $key ) {
          case 'license':
            $res = wp_set_object_terms( $post_id, intval($value), 'license', false);
            unset( $metadata[$key] ); // not needed to save in post_meta since we're using a taxonomy
            break;
          
          case 'project_phase':
            wp_set_object_terms( $post_id, intval($value), 'phase', false); 
            unset( $metadata[$key] ); // not needed to save in post_meta since we're using a taxonomy
            break;

          case 'project':
            wp_set_object_terms( $post_id, intval($value), 'project', false); 
            unset( $metadata[$key] ); // not needed to save in post_meta since we're using a taxonomy
            break;  
        }
      }       
      update_post_meta( $post_id, '_octoolbox_product_metadata', $metadata );  
    }
  }

  // add a license url to a license 
  function _render_license_url_input( $value = '' ) {
    return "<input type='text' value='$value' maxlength='250' size='40' name='license_url' id='license_url'></input>";    
  } 

  function new_license_url( $taxonomy ) {
    $html =  "<div class='license-url-wrap form-field'>";
    $html .= "<label for='license_url'>" . __('License URL') . "</label>";
    $html .= $this->_render_license_url_input(); 
    $html .= "<p>";
    $html .= __('The license URL should contain the canonical link for a license, e.g. https://creativecommons.org/licenses/by-sa/4.0/'); 
    $html .= "</p>";
    $html .=  "</div>";
    echo $html;
  }


  function save_license_url( $term_id, $tt_id) {
    if( isset( $_POST['license_url'] ) && ! empty( $_POST['license_url'] ) ){
        $license_url = esc_url( $_POST['license_url'] );
        update_term_meta( $term_id, 'license_url', $license_url );
    }
  }

  function edit_license_url( $term, $axonomy ) {

    $license_url = get_term_meta( $term->term_id, 'license_url', true );

    $html = "<tr class='form-field term-license-wrap'>"; 
    $html .= "<th scope='row'>";
    $html .= "<label>" . __('License URL') . "</label>";
    $html .= "</th>";
    $html .= "<td>"; 
    $html .= $this->_render_license_url_input( $license_url );
    $html .= "</td>"; 
    $html .= "</tr>"; 
    echo $html;    
  }

  function add_license_url_column( $columns ) {
    $columns[ 'license_url' ] = __('License URL');
    return $columns;
  }

  function add_license_url_column_content( $content, $col_name, $term_id ) {
    if( $col_name !== 'license_url' ) {
      return $content; 
    }

    $license_url = get_term_meta( $term_id, 'license_url', true ); 
    $content .= esc_url( $license_url ); 
    return $content; 
  }

} 
?>
