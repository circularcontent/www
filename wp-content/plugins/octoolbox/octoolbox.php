<?php
/**
 * Plugin Name:     OCToolbox
 * Plugin URI:      http://circularcontent.nl
 * Description:     Share creative assets & keep track of them
 * Author:          Bjorn Wijers <burobjorn at burobjorn dot nl>
 * Author URI:      http://burobjorn.nl
 * License:         GPLv2 or later
 * Text Domain:     octoolbox
 * Domain Path:     /languages
 * Version:         0.8-2017-03-29
 *
 */

if( ! class_exists( 'OCTOOLBOX' )) {
  class OCTOOLBOX {

    private $product; // reference product 
    private $project; 
    private $client; 
    private $person; 
    private $contract; 

    function __construct() {
      $this->setup_classes(); 
      $this->set_actions_filters();
    }

    function setup_classes() {
      require_once 'includes/class-product.php';
      require_once 'includes/class-project.php';
      require_once 'includes/class-person.php';
      require_once 'includes/class-client.php';
      require_once 'includes/class-contract.php';

      // class for helping with metabox rendering and common functions 
      require_once 'includes/class-util.php'; 


      $this->product = new OCProduct;
      $this->project = new OCProject;
      $this->person  = new OCPerson; 
      $this->client  = new OCClient; 
      $this->contract = new OCContract;

    }

    function set_actions_filters() {

      add_action( 'init', array( $this->product, 'cpt_oct_product_init') );
      add_action( 'init', array( $this->product, 'ct_oct_product_phase_init') );
      add_action( 'init', array( $this->product, 'ct_oct_product_license_init') );

      add_action( 'init', array( $this->client, 'cpt_oct_client_init') );
      add_action( 'init', array( $this->person, 'cpt_oct_person_init') );
      add_action( 'init', array( $this->person, 'ct_oct_person_product_init') );

      add_action( 'init', array( $this->project, 'cpt_oct_project_init') );
      add_action( 'init', array( $this->project, 'ct_oct_project_init') );

      add_action( 'init', array( $this->contract, 'cpt_oct_contract_init') );
      add_action( 'init', array( $this->contract, 'ct_oct_contract_person_init') );
      
      add_filter( 'post_updated_messages', array( $this->product, 'cpt_oct_product_updated_messages' ) );
      add_filter( 'post_updated_messages', array( $this->client, 'cpt_oct_client_updated_messages' ) );
      add_filter( 'post_updated_messages', array( $this->person, 'cpt_oct_person_updated_messages' ) );
      add_filter( 'post_updated_messages', array( $this->project, 'cpt_oct_project_updated_messages' ) );
      add_filter( 'post_updated_messages', array( $this->contract, 'cpt_oct_contract_updated_messages' ) );

      add_filter( 'enter_title_here', array( $this, 'custom_enter_title' ) );

      add_action( 'admin_enqueue_scripts',array( $this, 'enqueue_admin_scripts') );
      
      add_action( 'admin_menu', array( $this, 'add_settings_page' ) );
      add_action( 'admin_init', array( $this, 'settings_init' ) ); 

      add_action( 'admin_menu', array( $this, 'set_phases_menu' ) );
      add_action( 'admin_menu', array( $this, 'set_licenses_menu' ) );
      
      add_action( 'parent_file', array( $this, 'fix_octoolbox_settings_menu_highlight') );
    }

    function settings_init() {
	    register_setting( 'oct_settings_general', 'oct_settings' );

      add_settings_section(
        'oct_pluginPage_section', 
        __( 'Contracts', 'oct' ), 
        array( $this, 'oct_settings_section_callback'), 
        'oct_settings_general'
      );

      add_settings_field( 
        'oct_contract_text', 
        __( 'This text is used for creating contracts. Have a look at the help for more info.', 'oct' ), 
        array( $this, 'render_contract_text_field'), 
        'oct_settings_general', 
        'oct_pluginPage_section' 
      );
    }

    function render_contract_text_field() { 
      $html ='';
      $options = get_option( 'oct_settings' );
      $html .= "<textarea cols='80' rows='10' name='oct_settings[contract_text]'>";
      $html .= $options['contract_text'];
      $html .= "</textarea>";
      echo $html; 
    }

    function oct_settings_section_callback(  ) { 
      echo __( 'Alter the contracts text for your purpose' );
    }


    function add_settings_page() {
      add_menu_page(  __( 'OCToolbox Settings'), __( 'OC Toolbox Settings' ), 'manage_options', 'octoolbox_settings' , array( $this, 'render_settings_page' ), '', 81 );
    }

    function render_settings_page() {
      echo "<div class='wrap'>";
      echo  "<form action='options.php' method='post'>";
      echo  '<h1>' . __('OCToolbox Settings') . '</h1>';
      echo  '<p>';
      echo  __( 'Use this page to tune your Open Content Toolbox installation to your liking' );
      echo  '</p>';
      
      settings_fields( 'oct_settings_general' ); 
      do_settings_sections( 'oct_settings_general' );       

      submit_button();
      echo  "</form>";
      echo  "</div>";
    }

    function set_phases_menu() {
      add_submenu_page( 'octoolbox_settings', esc_html__( 'Project-phases' ), esc_html__( 'Project-phases' ), 'manage_options', 'edit-tags.php?taxonomy=phase' ); 
    }

    function set_licenses_menu() {
      add_submenu_page( 'octoolbox_settings', esc_html__( 'Licenses' ), esc_html__( 'Licenses' ), 'manage_options', 'edit-tags.php?taxonomy=license' ); 
    }
    
    function fix_octoolbox_settings_menu_highlight( $parent_file ) {
      if ( get_current_screen()->taxonomy == 'license' || get_current_screen()->taxonomy == 'phase' ) {
        $parent_file = 'octoolbox_settings';
      }
      return $parent_file;
    }


    function enqueue_admin_scripts() {
      wp_enqueue_script( 'octoolbox-admin', plugins_url('js/octoolbox-admin.js', __FILE__), array('jquery'), null , true);
    }


    // add the appropriate titles in the CPT title field
    function custom_enter_title( $input ) {
      global $post_type;

      if ( 'oct-client' === $post_type ) {
        return __( 'Enter organisation name here', 'YOUR-TEXTDOMAIN' );
      }

      if ( 'oct-project' === $post_type ) {
        return __( 'Enter project title here', 'YOUR-TEXTDOMAIN' );
      }

      if ( 'oct-product' === $post_type ) {
        return __( 'Enter product title here', 'YOUR-TEXTDOMAIN' );
      }

      if ( 'oct-person' === $post_type ) {
        return __( 'Enter the full name of the person', 'YOUR-TEXTDOMAIN' );
      }

      return $input;
    }



  }
  $octoolbox = new OCTOOLBOX;  
} else {
  error_log( ' Could not instantiate OCTOOLBOX class, due to the class already existing' ); 
}
?>
