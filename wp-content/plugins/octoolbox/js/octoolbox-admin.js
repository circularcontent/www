jQuery(document).ready(function($) {
  
  // Remove row added by js, which is not in the database yet
  $('body').on('click', '.octoolbox-remove-js', function(e) { 
    $( this ).closest( 'tr' ).remove(); 
    console.log( 'remove' + this.tagName ); 
  });
  

  // Add a new row of values
  // #TODO Hide the values into input boxes
  $('body').on('click', '#octoolbox_project_add_nw_product', function(e) {
      var nw_row = "<tr>";
      var missing_data = false;
      $('table#octoolbox_project_products :input:not(:button, :hidden, :checkbox)').each( function(){
        if( 'required' == $( this ).attr( 'data-required' ) && ! $( this ).val() ) {
          $( this ).css( 'border','1px solid #F00');
          missing_data = true;
        } else {
          console.log( this.tagName ); 
          $( this ).css( 'border','1px solid #999');  
          nw_row += "<td class='not-saved'>";
          switch ( this.tagName ) {
            case 'SELECT':
              var select_id = "#" + $( this ).attr( 'id' ) + " option:selected";
              nw_row += $( select_id ).text();
              break;
            case 'INPUT':
              nw_row += $( this ).val();
              break;
          }
        
          nw_row += add_hidden_value( $( this ).attr( 'name' ), $( this).val() );
          nw_row += "</td>";
        }
      }); 
      if( missing_data ) {
        alert( 'Missing mandatory data. Fields missing data have a red border' );
      } else {
        nw_row +="<td>Save or update your project to add this product</td>";
        nw_row +="<td></td>";
        nw_row +="<td><input type='button' value='Remove' class='button octoolbox-remove-js'></input></td>";
        nw_row += "</tr>";
        $( nw_row ).appendTo('table#octoolbox_project_products');
      }
  });  


  $('body').on('click', '#octoolbox_project_add_person', function(e) {
      var nw_row = "<tr>";
      var add_people_name = 'existing_people_ids[]';
      var person_id   = $( '#oct-product-team-member' ).val()
      var person_name = $( '#oct-product-team-member option:selected' ).text();

      nw_row += "<td>";
      nw_row += person_name;
      nw_row += "</td>";

      nw_row += "<td>Save or update your product to add this person</td>";
      nw_row += add_hidden_value( add_people_name, person_id );
      nw_row +="<td></td>";
      nw_row +="<td><input type='button' value='Remove' class='button octoolbox-remove-js'></input></td>";
      
      nw_row += "</tr>";
      $( nw_row ).appendTo('table#octoolbox_product_team');

  });



  function add_hidden_value( name, value ) {
    return "<input type='hidden' name='" + name + "' value='" + value + "'>"; 
  } 
});



/** 
 *
 *
    var product_data = $('table#octoolbox_project_products :input').serializeArray(); 
    var data = {
      action: 'octoolbox_project_add_product',
      product: product_data
    }
    
    // ajaxurl by default exists for wp-admin since 2.8 
    $.post( ajaxurl, data, function( response )  {
      if( response == '0' || response == '-1' ) {
        console.log( 'response was in error' ); 
      } else {
        console.log( 'response was ok ' ); 
      }
    });  

  
  });

  **/
